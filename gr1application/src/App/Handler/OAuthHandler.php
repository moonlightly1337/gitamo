<?php

declare(strict_types=1);

namespace App\Handler;
use Laminas\Diactoros\Response\HtmlResponse;
use Laminas\Diactoros\Response\JsonResponse;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

use function time;

class OAuthHandler implements RequestHandlerInterface
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $clientId = 'cc797885-6c32-4959-9f12-af917b055107';
        $clientSecret = 'RdaZZumu17Ybg4k5gc9MFhNq1aRq2tfQQQ7zDfuj52DQuZgILT6k4eFnUWK36ALs';
        $redirectURI = 'https://webhook.site/ca681646-d978-4413-80cf-aaa851e9f674';
        $apiClient = new \AmoCRM\Client\AmoCRMApiClient($clientId, $clientSecret, $redirectURI);
        $state = bin2hex(random_bytes(16));
        /*
        echo $apiClient->getOAuthClient()->getOAuthButton(
            [
                'title' => 'Установить интеграцию',
                'compact' => true,
                'class_name' => 'className',
                'color' => 'default',
                'error_callback' => 'handleOauthError',
                'state' => $state,
            ]
        );
        */
        $authorizationUrl = $apiClient->getOAuthClient()->getAuthorizeUrl([
            'state' => $state,
            'mode' => 'post_message',
        ]);
        header('Location: ' . $authorizationUrl);
        die;
        return new JsonResponse(['owner details' => 'ok']);
    }
}
